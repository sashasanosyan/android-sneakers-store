package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.myapplication.model.Category;
import com.example.myapplication.model.Data;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private Data data;
    private List<Category> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         listView = findViewById(R.id.list_categories);

         Data data = Data.getInstance();

         categories = data.getCategories();

         CustomAdapter customAdapter = new CustomAdapter();
         listView.setAdapter(customAdapter);


         listView.setOnItemClickListener((parent, view, position, id) -> {
             Intent intent = new Intent(getApplicationContext(), ItemListActivity.class);
             intent.putExtra("logo", categories.get(position).getSrc());
             intent.putExtra("category", categories.get(position).getName());
             startActivity(intent);
         });




    }

    public void OnCartClick(View view) {
        Intent intent2 = new Intent(this, CartActivity.class);
        startActivity(intent2);
    }

    public void OnAddNewClick(View view) {
        Intent intent1 = new Intent(this, AddCategory.class);
        startActivity(intent1);
    }

    public void OnClickRate(View view) {
        Intent intent3 = new Intent(this, RatingActivity.class);
        startActivity(intent3);
    }

    private class CustomAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return categories.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.activity_category, null);
            ImageView imageView = view.findViewById(R.id.category_item_logo);

            imageView.setImageResource(categories.get(position).getSrc());
            return view;
        }
    }
}
