package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.myapplication.model.Category;
import com.example.myapplication.model.Data;
import com.example.myapplication.model.Item;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity {
    private ImageView imageView;
    private ListView listView;
    private Data data;
    private List<Item> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        data = Data.getInstance();


        imageView = findViewById(R.id.category_item_logo);
        Intent intent = getIntent();
        int image = intent.getIntExtra("logo", 0);
        imageView.setImageResource(image);

        String category = intent.getStringExtra("category");
        Category category1 = data.getCatByName(category);


    }

}