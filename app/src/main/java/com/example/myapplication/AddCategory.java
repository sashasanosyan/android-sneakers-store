package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.model.Category;
import com.example.myapplication.model.Data;

import java.util.ArrayList;
import java.util.List;

public class AddCategory extends AppCompatActivity {

    private Button submit;
    private List<Category> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        final EditText name = (EditText) findViewById(R.id.editText);
        submit = (Button)findViewById(R.id.submit_button);

        Data data = Data.getInstance();

        submit.setOnClickListener(v -> {
            if (name.getText().toString().isEmpty()){
                Toast.makeText(getApplicationContext(), "Enter the data", Toast.LENGTH_SHORT).show();
            } else {
                String enteredName = name.getText().toString();
                data.addNewCategory(enteredName);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });


    }
}