package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

public class Item_list_single extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list_single);

        imageView = findViewById(R.id.item_list_single_pic);
        Intent intent = getIntent();
        int image = intent.getIntExtra("logo", 0);
        imageView.setImageResource(image);
    }
}