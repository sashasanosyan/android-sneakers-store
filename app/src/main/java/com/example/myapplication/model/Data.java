package com.example.myapplication.model;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.List;
import android.content.res.Resources;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;

public class Data {

    private static Data instance = null;

    private final List<Category> categories;
    private List<Item> items;
    private final List<Item> cart;

    private double rate = 0;

    public synchronized static Data getInstance() {
        if(instance == null) {
            instance = new Data();
        }
        return instance;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        if(rate < 0 || rate > 5) return;
        this.rate = rate;
    }

    private Data(){
        this.categories = new ArrayList<>();
        this.items = new ArrayList<>();
        this.cart = new ArrayList<>();
        init();

    }

    public boolean addToCart(Item item) {
        if (this.cart.contains(item)) {
            return false;
        } else {
            this.cart.add(item);
            return true;
        }
    }

    private void init()
    {
        updateCategories();
    }

    private void updateCategories() {
        categories.add(new Category("Jeans", R.drawable.jeans_logo));
        categories.add(new Category("T-shirts", R.drawable.tshirt_logo));
        categories.add(new Category("Sweatshirts", R.drawable.sweatshirt_logo));
        categories.add(new Category("Jackets", R.drawable.jacket_logo));
        categories.add(new Category("Dresses", R.drawable.dress_logo));
    }

    public List<Item> getCart() {
        return cart;
    }


    public void addToCart(int imgsrc){
        cart.add(new Item(null, null, null, imgsrc));
    }


    public Category getCatByName(String name){
        Category category = null;
        for (Category c: categories){
            if (c.getName().equals(name)){
                category = c;
            }
        }
        return category;
    }


    public List<Item> addItems(List<Item> items){
        items.add(new Item("Jeans", "Black jeans", "30$", R.drawable.black_jeans));
        items.add(new Item("Jeans", "Blue jeans", "40$", R.drawable.blue_jeans));
        items.add(new Item("Jeans", "White jeans", "45$", R.drawable.white_jeans));
        items.add(new Item("Jeans", "Grey jeans", " 50$", R.drawable.grey_jeans));

        items.add(new Item("T-shirts", "Black t-shirt", "10$", R.drawable.black_tshirt));
        items.add(new Item("T-shirts", "Orange t-shirt", "15$", R.drawable.orange_tshirt));
        items.add(new Item("T-shirts", "Pink t-shirt", "20$", R.drawable.pink_tshirt));
        items.add(new Item("t-shirts", "White t-shirt", "15$", R.drawable.white_tshirt));

        items.add(new Item("Sweatshirts", "Beige hoodie", "40$", R.drawable.beige_hoodie));
        items.add(new Item("Sweatshirts", "Black sweatshirt", "25$", R.drawable.black_sweatshirt));
        items.add(new Item("Sweatshirts", "Pink hoodie", "440$", R.drawable.pink_hoodie));

        items.add(new Item("Jackets", "Big swoosh nike jacket", "150$", R.drawable.nike_jacket));
        items.add(new Item("Jackets", "Puffer jacket lilac", "90$", R.drawable.puffer_jacket));
        items.add(new Item("Jackets", "Bomber jacket", "60$", R.drawable.bomber_jacket));
        items.add(new Item("Jackets", "Hoodie jacket", "30$", R.drawable.hoodie_jacket));

        items.add(new Item("Dresses", "Blue dress", "40$", R.drawable.blue_dress));
        items.add(new Item("Dresses", "Black dress", "50$", R.drawable.black_dress));
        items.add(new Item("Dresses", "Red dress", "30$", R.drawable.red_dress));
        items.add(new Item("Dresses", "blue dress with dots", "30$", R.drawable.dot_dress));

        return items;
    }

    public void addNewCategory(String name){
        categories.add(new Category(name, R.drawable.new_category));
    }

    public List<Item> returnItemsPerCategory(String categoryName){
        List<Item> allItems = new ArrayList<>();
        List<Item> categoryItems = new ArrayList<>();

        allItems = addItems(allItems);
        for (Item i: allItems){
            if (i.getCategory().equals(categoryName)){
                categoryItems.add(i);
            }
        }
        return categoryItems;
    }



    public Item getItemsByPic(int src){
        List<Item> allItems = new ArrayList<>();
        Item item = null;

        allItems = addItems(allItems);
        for (Item i: allItems){
            if (i.getPicturesrc() == src){
                item = i;
            }
        }

        return item;
    }

    public List<Category> getCategories() {
        return categories;
    }

    @NonNull
    @Override
    public String toString() {
        return cart.toString();
    }
}
