package com.example.myapplication.model;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private String name;
    private List<Item> items;

    private int src;

    public Category(String name, int src){
        this.name = name;
        this.src = src;
    }

    public Category(String name){
        this.name = name;
        this.items = new ArrayList<>();
    }

    public int getSrc() {
        return src;
    }

    public List<Item> getItems() {
        return items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addNewItem(Item item){
        this.items.add(item);
    }

}

