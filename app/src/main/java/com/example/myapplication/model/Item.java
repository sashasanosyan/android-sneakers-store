package com.example.myapplication.model;


import androidx.annotation.NonNull;

public class Item {
    private String name;
    private String price;
    private int picturesrc;
    private String category;

    public Item(String category, String name, String price, int picturesrc){
        this.category = category;
        this.picturesrc = picturesrc;
        this.name = name;
        this.price = price;
    }


    public String getCategory() {
        return category;
    }

    public String getPrice() {
        return price;
    }

    public int getPicturesrc() {
        return picturesrc;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPicturesrc(int picturesrc) {
        this.picturesrc = picturesrc;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @NonNull
    @Override
    public String toString() {
        return "{name: " + name + ", " + "price: " + price + "}";
    }
}

