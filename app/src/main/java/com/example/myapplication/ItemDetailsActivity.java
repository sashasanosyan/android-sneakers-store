package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.model.Data;
import com.example.myapplication.model.Item;

import java.util.List;

public class ItemDetailsActivity extends AppCompatActivity {
    private ImageView imageView;
    private CustomView customView;
    private Button cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        imageView = findViewById(R.id.imageView_item);
        Intent intent = getIntent();
        int image = intent.getIntExtra("pic", 0);
        imageView.setImageResource(image);

        customView = findViewById(R.id.heart_custom);

        customView.setOnClickListener(v -> {
            customView.fillHeart();
            customView.invalidate();

            Data data = Data.getInstance();
            data.addToCart(image);

            Log.i("TAG", "onClick:" + data.toString());

            Intent intent1 = new Intent(getApplicationContext(), CartActivity.class);
            startActivity(intent1);
        });

        cart = findViewById(R.id.open_cart);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent2);
            }
        });

    }


}