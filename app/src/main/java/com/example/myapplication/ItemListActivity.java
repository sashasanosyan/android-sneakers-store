package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.myapplication.model.Category;
import com.example.myapplication.model.Data;
import com.example.myapplication.model.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemListActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView textView;
    private ListView listView;
    private Data data;
    private List<Item> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        imageView = findViewById(R.id.imageView_category_pic);
        textView = findViewById(R.id.category_name);
        Intent intent = getIntent();
        int image = intent.getIntExtra("logo", 0);
        imageView.setImageResource(image);

        String categoryName = intent.getStringExtra("category");
        textView.setText(categoryName);


        listView = findViewById(R.id.list_of_items);
        items = new ArrayList<>();
        data = Data.getInstance();

        Category current = data.getCatByName(categoryName);

        items = data.returnItemsPerCategory(categoryName);


        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent1 = new Intent(getApplicationContext(), ItemDetailsActivity.class);
            intent1.putExtra("pic", items.get(position).getPicturesrc());

            startActivity(intent1);
        });


    }

    public class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.activity_item_list_single, null);
            ImageView imageView = view.findViewById(R.id.item_list_single_pic);
            TextView textView = view.findViewById(R.id.textView_name);

            imageView.setImageResource(items.get(position).getPicturesrc());
            textView.setText(items.get(position).getName());
            return view;
        }
    }


}






