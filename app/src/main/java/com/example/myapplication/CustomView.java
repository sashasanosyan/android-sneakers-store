package com.example.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

public class CustomView extends View {

    private Path path;
    private Paint heart_outline_paint;

    public CustomView(Context context) {
        super(context);

        init(null);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(attrs);
    }

    private void init(@Nullable AttributeSet set){

        heart_outline_paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        heart_outline_paint.setStrokeJoin(Paint.Join.MITER);
        heart_outline_paint.setColor(Color.RED);
        heart_outline_paint.setStrokeWidth(4);
        heart_outline_paint.setStyle(Paint.Style.STROKE);
        path = new Path();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float length = 70;
        float x = canvas.getWidth()/2;
        float y = canvas.getHeight()/2;

        canvas.rotate(45,x,y);

        path.moveTo(x,y);
        path.lineTo(x-length, y);
        path.arcTo(new RectF(x-length-(length/2),y-length,x-(length/2),y),90,180);
        path.arcTo(new RectF(x-length,y-length-(length/2),x,y-(length/2)),180,180);
        path.lineTo(x,y);
        path.close();

        canvas.drawPath(path, heart_outline_paint);
    }

    public void fillHeart() {
        if (heart_outline_paint.getStyle() == Paint.Style.FILL_AND_STROKE) {
            heart_outline_paint.setStyle(Paint.Style.STROKE);
        } else if (heart_outline_paint.getStyle() == Paint.Style.STROKE) {
            heart_outline_paint.setStyle(Paint.Style.FILL_AND_STROKE);
        }

    }
}
