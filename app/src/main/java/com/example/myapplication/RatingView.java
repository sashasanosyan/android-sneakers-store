package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.myapplication.model.Data;

public class RatingView extends ConstraintLayout {
    private TextView textView;
    private RatingBar ratingBar;
    private Button rateButton;

    public RatingView(@NonNull Context context) {
        super(context);

        init(context);
    }

    public RatingView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    public RatingView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    public RatingView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(context);
    }

    public void init(Context context){
        View v = LayoutInflater.from(context)
                .inflate(R.layout.activity_rating, this, true);

        textView = v.findViewById(R.id.textView5);
        ratingBar = v.findViewById(R.id.ratingBar2);
        rateButton = v.findViewById(R.id.rate_button);

        rateButton.setOnClickListener(view -> {
            updateRating();
        });

        initRating();
    }

    private void initRating() {
        double rate = Data.getInstance().getRate();
        setTextRating(rate);
        ratingBar.setRating((float) rate);
    }

    private void updateRating() {
        double rate = ratingBar.getRating();
        Data.getInstance().setRate(rate);
        setTextRating(rate);
    }

    @SuppressLint("SetTextI18n")
    public void setTextRating(double rating){
        Log.i("TAG", "setTextRating: ");
        textView.setText("Your rate mark is: " + rating);
    }
}